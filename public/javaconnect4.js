var p4 = [[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]
// ^ On créé un tableau double dimension JS


// Au chargement de la page
var game = 1 // Partie en cours
var firststart = 1 // Qui a commencé en premier? 1 ou 2
var turn = 1 // A qui le tour 
var firstplayed = false // Partie a déjà commencé?

var p1score = 0
var p2score = 0

var player1Name = 'J1'
var player2Name = 'J2'

function clicked(number)
{
	if (game == 1)
	{
		for (var i = 0; i < 7; i++) // D'abord, on vérfie si un joueur peut mettre un jeton dans une colonne | Verifier si la colonne est remplie
		{
			//console.log(i + ' ' + number)
			if (p4[i][number-1] == 0)
			{
				p4[i][number-1] = turn
				//console.log('test' + number)
				console.log(p4)
				
				firstplayed = true
				
				var lig = 7-(i+1)
				var col = number
				//console.log('' + lig + '-' + col)
				
				if (turn == 1) // Quand le tour est joué, au tour de l'adversaire
				{
					document.getElementById('turned').innerHTML = 'Au tour de ' + player2Name;
					document.getElementById('turned').style.color = '#ff0000';
					document.getElementById( lig + '-' + col ).src = "images/connect4/yellowcircle.png"
					turn = 2
				}
				else
				{
					document.getElementById('turned').innerHTML = 'Au tour de ' + player1Name;
					document.getElementById('turned').style.color = '#ffcc00';
					document.getElementById( lig + '-' + col ).src = "images/connect4/redcircle.png"
					turn = 1
				}
				
				// Verification (Victoires, égalités)
				
				for (var i = 0; i < 8; i++)
				{
					for (var j = 0; j < 7; j++)
					{
						
						// J1 -- Horizontale, Verticale, Diagonale vers la droite, Diagonale vers la gauche
						if (((p4[i][j] == 1 && p4[i+1][j] == 1 && p4[i+2][j] == 1 && p4[i+3][j] == 1) || (p4[i][j] == 1 && p4[i][j+1] == 1 && p4[i][j+2] == 1 && p4[i][j+3] == 1) || (p4[i][j] == 1 && p4[i+1][j+1] == 1 && p4[i+2][j+2] == 1 && p4[i+3][j+3] == 1) || (p4[i][j] == 1 && p4[i+1][j-1] == 1 && p4[i+2][j-2] == 1 && p4[i+3][j-3] == 1)) && game == 1)
						{
							//alert('p1')
							game = 0
							firstplayed = false
							document.getElementById('turned').innerHTML = player1Name + ' a gagné cette manche!';
							document.getElementById('turned').style.color = '#ffcc00';
							p1score = p1score + 1
							document.getElementById('score').innerHTML = p1score + '-' + p2score;
							document.getElementById('rematch').innerHTML = "<input type='button' onclick='newRound()' value='Revanche!'>"
						}
						
						// J2
						if (((p4[i][j] == 2 && p4[i+1][j] == 2 && p4[i+2][j] == 2 && p4[i+3][j] == 2) || (p4[i][j] == 2 && p4[i][j+1] == 2 && p4[i][j+2] == 2 && p4[i][j+3] == 2) || (p4[i][j] == 2 && p4[i+1][j+1] == 2 && p4[i+2][j+2] == 2 && p4[i+3][j+3] == 2) || (p4[i][j] == 2 && p4[i+1][j-1] == 2 && p4[i+2][j-2] == 2 && p4[i+3][j-3] == 2)) && game == 1)
						{
							//alert('p2')
							game = 0
							firstplayed = false
							document.getElementById('turned').innerHTML = player2Name + ' a gagné cette manche!';
							document.getElementById('turned').style.color = '#ff0000';
							p2score = p2score + 1
							document.getElementById('score').innerHTML = p1score + '-' + p2score;
							document.getElementById('rematch').innerHTML = "<input type='button' onclick='newRound()' value='Revanche!'>"
							//alert('ok')
						}
						
						// Egalité -- On a juste a vérifier si les lignes du haut sont remplies
						if (p4[5][0] != 0 && p4[5][1] != 0 && p4[5][2] != 0 && p4[5][3] != 0 && p4[5][4] != 0 && p4[5][5] != 0 && p4[5][6] != 0 && game == 1)
						{
							game = 0;
							firstplayed = false
							document.getElementById('turned').innerHTML = 'Egalité!';
							document.getElementById('turned').style.color = '#aaaaaa';
							document.getElementById('rematch').innerHTML = "<input type='button' onclick='newRound()' value='Revanche!'>"
						}
					}
				}
			}
		}
    }
}




// New Round & New Challenger

function newRound()
{
	if (firststart == 1)
	{
		firststart = 2
		document.getElementById('turned').innerHTML = 'Au tour de ' + player2Name;
		document.getElementById('turned').style.color = '#ff0000';
	}
	else
	{
		firststart = 1
		document.getElementById('turned').innerHTML = 'Au tour de ' + player1Name;
		document.getElementById('turned').style.color = '#ffcc00';
	}
	
	p4 = [[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]
	turn = firststart
	game = 1
	firstplayed = false
	document.getElementById('rematch').innerHTML = "<p></p>"
	
	for (var i = 0; i < 8; i++)
	{
		for (var j = 0; j < 7; j++)
		{
			lig = i+1
			col = j+1
			document.getElementById( lig + '-' + col ).src = "images/connect4/whitecircle.png"
		}
	}
}

function newChallenger()
{
	if (firstplayed == false)
	{
		p1score = 0
		p2score = 0
		document.getElementById('score').innerHTML = p1score + '-' + p2score;
		newRound()
	}
}

// Name Functions

function p1name()
{
	if (firstplayed == false && document.getElementById('p1').value != '' && document.getElementById('p1').value != player1Name)
	{
		player1Name = document.getElementById('p1').value;
		alert("Nouveau Joueur 1 : " + player1Name); // Fun fact : On ne peut pas appeler la valeur "p1name" car ça appelle la fonction
		newChallenger()
	}
}

function p2name()
{
	if (firstplayed == false && document.getElementById('p2').value != '' && document.getElementById('p2').value != player2Name)
	{
		player2Name = document.getElementById('p2').value;
		alert("Nouveau Joueur 2 : " + player2Name);
		newChallenger()
	}
}