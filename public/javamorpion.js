// Au chargement de la page
var game = 1 // Partie en cours ou non
var firststart = 'O' // Qui commence en premier
var turn = 'O' // A qui le tour
var firstplayed = false // Match a commencé

var button1 = '' // O ou X
var button2 = ''
var button3 = ''
var button4 = ''
var button5 = ''
var button6 = ''
var button7 = ''
var button8 = ''
var button9 = ''

var p1score = 0
var p2score = 0

var player1Name = 'J1'
var player2Name = 'J2'

function clicked(number) // Utilisé lorsque un bouton est cliqué -- La fonction vérifie plusieurs choses
{
    if (document.getElementById('b' + number).innerHTML == '　' && game == 1) // Vérifie que la partie est en cours et que la case n'est pas encore utilisé
	{
		firstplayed = true // Voir p1name, p2name, ect...
        document.getElementById('b' + number).innerHTML = turn;
        if (turn == 'O') // Après le tour joué, au tour de l'adversaire
		{
            turn = 'X';
			document.getElementById('turned').innerHTML = 'Au tour de ' + player2Name + ' (X)';
			document.getElementById('turned').style.color = '#ffaaaa';
        } else if (turn == 'X')
		{
            turn = 'O';
			document.getElementById('turned').innerHTML = 'Au tour de ' + player1Name + ' (O)';
			document.getElementById('turned').style.color = '#aaaaff';
        }
        // console.log(document.getElementById('b' + number).innerHTML);
        console.log(turn);
    }
	
	// Verification -- On vérifie si la partie est finie
	// Verif J1 (Pour voir si il a gagné)
	
	// Verif Horizontale, Veritcale, Diagonale
	if (((document.getElementById('b1').innerHTML == 'O' && document.getElementById('b4').innerHTML == 'O' && document.getElementById('b7').innerHTML == 'O')
		|| (document.getElementById('b2').innerHTML == 'O' && document.getElementById('b5').innerHTML == 'O' && document.getElementById('b8').innerHTML == 'O')
		|| (document.getElementById('b3').innerHTML == 'O' && document.getElementById('b6').innerHTML == 'O' && document.getElementById('b9').innerHTML == 'O')
		
		|| (document.getElementById('b1').innerHTML == 'O' && document.getElementById('b2').innerHTML == 'O' && document.getElementById('b3').innerHTML == 'O')
		|| (document.getElementById('b4').innerHTML == 'O' && document.getElementById('b5').innerHTML == 'O' && document.getElementById('b6').innerHTML == 'O')
		|| (document.getElementById('b7').innerHTML == 'O' && document.getElementById('b8').innerHTML == 'O' && document.getElementById('b9').innerHTML == 'O')
		
		|| (document.getElementById('b1').innerHTML == 'O' && document.getElementById('b5').innerHTML == 'O' && document.getElementById('b9').innerHTML == 'O')
		|| (document.getElementById('b3').innerHTML == 'O' && document.getElementById('b5').innerHTML == 'O' && document.getElementById('b7').innerHTML == 'O'))
		
		&& game == 1)
	{
		game = 0
		document.getElementById('turned').innerHTML = player1Name + ' a gagné cette manche!';
		document.getElementById('turned').style.color = '#aaaaff';
		p1score = p1score + 1 // Ajoute un point au Joueur 1
		//alert('ok')
	}
	
	// Verif P2, on fait la même chose (Pas de "else if" car j'utilise "game == 1")
	if (((document.getElementById('b1').innerHTML == 'X' && document.getElementById('b4').innerHTML == 'X' && document.getElementById('b7').innerHTML == 'X')
		|| (document.getElementById('b2').innerHTML == 'X' && document.getElementById('b5').innerHTML == 'X' && document.getElementById('b8').innerHTML == 'X')
		|| (document.getElementById('b3').innerHTML == 'X' && document.getElementById('b6').innerHTML == 'X' && document.getElementById('b9').innerHTML == 'X')
		
		|| (document.getElementById('b1').innerHTML == 'X' && document.getElementById('b2').innerHTML == 'X' && document.getElementById('b3').innerHTML == 'X')
		|| (document.getElementById('b4').innerHTML == 'X' && document.getElementById('b5').innerHTML == 'X' && document.getElementById('b6').innerHTML == 'X')
		|| (document.getElementById('b7').innerHTML == 'X' && document.getElementById('b8').innerHTML == 'X' && document.getElementById('b9').innerHTML == 'X')
		
		|| (document.getElementById('b1').innerHTML == 'X' && document.getElementById('b5').innerHTML == 'X' && document.getElementById('b9').innerHTML == 'X')
		|| (document.getElementById('b3').innerHTML == 'X' && document.getElementById('b5').innerHTML == 'X' && document.getElementById('b7').innerHTML == 'X'))
		
		&& game == 1)
	{
		game = 0
		document.getElementById('turned').innerHTML = player2Name + ' a gagné cette manche!';
		document.getElementById('turned').style.color = '#ffaaaa';
		p2score = p2score + 1
		//alert('ok')
	}
	// Egalité -- Verifie si toutes les cases sont remplies ou non
	if (document.getElementById('b1').innerHTML != '　' && document.getElementById('b2').innerHTML != '　' && document.getElementById('b3').innerHTML != '　'
		&& document.getElementById('b4').innerHTML != '　' && document.getElementById('b5').innerHTML != '　' && document.getElementById('b6').innerHTML != '　'
		&& document.getElementById('b7').innerHTML != '　' && document.getElementById('b8').innerHTML != '　' && document.getElementById('b9').innerHTML != '　'
		
		&& game == 1)
	{
		game = 0
		document.getElementById('turned').innerHTML = 'Egalité!';
		document.getElementById('turned').style.color = '#aaaaaa';
		//alert('ok')
	}
	
	// fin
	if (game == 0) // On met a jour le score, et on fait apparaitre le bouton "Revanche"
	{
		document.getElementById('score').innerHTML = p1score + '-' + p2score;
		document.getElementById('rematch').innerHTML = "<input type='button' onclick='newRound()' value='Revanche!'>"
		firstplayed = false
	}
}


// New Round & New Challenger

function newRound() // Utilisé par le bouton "Revance" pour recommencer un match
{
	//game = 0
	p1turn = 0
	p2turn = 0	
	document.getElementById('rematch').innerHTML = "<p></p>"
	document.getElementById('b1').innerHTML = '　'
	document.getElementById('b2').innerHTML = '　'
	document.getElementById('b3').innerHTML = '　'
	document.getElementById('b4').innerHTML = '　'
	document.getElementById('b5').innerHTML = '　'
	document.getElementById('b6').innerHTML = '　'
	document.getElementById('b7').innerHTML = '　'
	document.getElementById('b8').innerHTML = '　'
	document.getElementById('b9').innerHTML = '　'
	
	if (firststart == 'O') // Ce n'est pas la même personne qui commence
	{
		firststart = 'X'
		document.getElementById('turned').innerHTML = 'Au tour de ' + player2Name + ' (X)';
		document.getElementById('turned').style.color = '#ffaaaa';
	}
	else
	{
		firststart = 'O'
		document.getElementById('turned').innerHTML = 'Au tour de ' + player1Name + ' (O)';
		document.getElementById('turned').style.color = '#aaaaff';
		//document.getElementById('turned').innerHTML = '<h2 id="turned">Au tour de ' + player1Name + ' (O)</h2>'
	}
	turn = firststart
	console.log(document.getElementById('turned').style.color)
	game = 1
	firstplayed = false
}

function newChallenger() // Voir d'abord p1name et p2name
{
	if (firstplayed == false) // newRound() mais on réinitialise le Score car c'est une Nouvelle Personne qui joue
	{
		newRound()
		p1score = 0
		p2score = 0
		document.getElementById('score').innerHTML = p1score + '-' + p2score;
	}
}

function p1name() // On peut changer de nom pour si jamais plus de 2 personnes sont sur un PC
{
	if (firstplayed == false && document.getElementById('p1').value != '' && document.getElementById('p1').value != player1Name) // Conditions pour changer son nom : Le nom ne peut pas être vide, Le nom que vous mettez n'est pas déjà utilisé par vous (C'est comme faire F5), Il faut que la partie soit finie ou n'a pas encore commencé (Pour éviter les RQ)
	{
		player1Name = document.getElementById('p1').value;
		alert("Nouveau Joueur 1 : " + player1Name); // || Au passage : On ne peut pas appeler la valeur "p1name" car ça appelle la fonction
		newChallenger()
	}
}

function p2name()
{
	if (firstplayed == false && document.getElementById('p2').value != '' && document.getElementById('p2').value != player2Name)
	{
		player2Name = document.getElementById('p2').value;
		alert("Nouveau Joueur 2 : " + player2Name);
		newChallenger()
	}
}