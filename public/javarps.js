// Au chargement de la page
var game = 1 // Partie en cours ou non
var p1ready = 0 // Pret ou pas
var p2ready = 0
var p1choice = '' // Choix
var p2choice = ''
var player1Name = 'J1'
var player2Name = 'J2'
var p1score = 0
var p2score = 0

document.addEventListener('keydown', function(event) { // Ecouteur d'évenement : Surveille des evenements comme ex: un clique gauche)
    if (document.activeElement.tagName !== 'INPUT') // Pour pas que la page détecte les touches de clavier pendant qu'on change de nom dans les inputs
	{
		// --- Détection touches du clavier --- \\
		// P1
        if ((event.key == 'q' || event.key == 'Q') && p1ready == 0) // On vérifie le choix prit avec la touche de clavier || Miniscules et Majuscules ne sont pas la même chose
		{
            p1choice = 'Pierre';
            p1ready = 1;
			document.getElementById('waitp1').innerHTML = "<strong>[Prêt(e)]</strong>";
			console.log(player1Name + ' a choisi Pierre')
        }
		else if ((event.key == 's' || event.key == 'S') && p1ready == 0)
		{
            p1choice = 'Feuille';
            p1ready = 1;
			document.getElementById('waitp1').innerHTML = "<strong>[Prêt(e)]</strong>";
			console.log(player1Name + ' a choisi Feuille')
        }
		else if ((event.key == 'd' || event.key == 'D') && p1ready == 0)
		{
            p1choice = 'Ciseaux';
            p1ready = 1;
			document.getElementById('waitp1').innerHTML = "<strong>[Prêt(e)]</strong>";
			console.log(player1Name + ' a choisi Ciseaux')
        }
		
		// P2
		if (event.key == 'ArrowLeft' && p2ready == 0)
		{
            p2choice = 'Pierre';
            p2ready = 1;
			document.getElementById('waitp2').innerHTML = "<strong>[Prêt(e)]</strong>";
			console.log(player2Name + ' a choisi Pierre')
        }
		else if (event.key == 'ArrowDown' && p2ready == 0)
		{
            p2choice = 'Feuille';
            p2ready = 1;
			document.getElementById('waitp2').innerHTML = "<strong>[Prêt(e)]</strong>";
			console.log(player2Name + ' a choisi Feuille')
        }
		else if (event.key == 'ArrowRight' && p2ready == 0)
		{
            p2choice = 'Ciseaux';
            p2ready = 1;
			document.getElementById('waitp2').innerHTML = "<strong>[Prêt(e)]</strong>";
			console.log(player2Name + ' a choisi Ciseaux')
        }
    }
	
	// --- Choix & Images --- \\ -- Partie Visuelle
	
	if (p1ready == 1 && p2ready == 1 && game == 1)
	{
		document.getElementById('waitp1').innerHTML = "";
		document.getElementById('waitp2').innerHTML = "";
		game = 0
		
		// Check Choices
		
		if (p1choice == 'Pierre')
		{
			document.getElementById('choosingP1').src = "images/rps/Pierre.png"
		}
		else if (p1choice == 'Feuille')
		{
			document.getElementById('choosingP1').src = "images/rps/Feuille.png"
		}
		else if (p1choice == 'Ciseaux')
		{
			document.getElementById('choosingP1').src = "images/rps/Ciseaux.png"
		}
		
		if (p2choice == 'Pierre')
		{
			document.getElementById('choosingP2').src = "images/rps/Pierre.png"
		}
		else if (p2choice == 'Feuille')
		{
			document.getElementById('choosingP2').src = "images/rps/Feuille.png"
		}
		else if (p2choice == 'Ciseaux')
		{
			document.getElementById('choosingP2').src = "images/rps/Ciseaux.png"
		}
	
		// --- Résultats --- \\
		
		if (p1choice == p2choice)
		{
			document.getElementById('rpsinstruction').innerHTML = "Egalité!";
		}
		else if ((p1choice == 'Pierre' && p2choice == 'Ciseaux') || (p1choice == 'Feuille' && p2choice == 'Pierre') || (p1choice == 'Ciseaux' && p2choice == 'Feuille'))
		{
			document.getElementById('rpsinstruction').innerHTML = player1Name + " a gagné cette manche!";
			document.getElementById('rpsinstruction').style.color = '#aaaaff';
			p1score = p1score + 1
		}
		else if ((p2choice == 'Pierre' && p1choice == 'Ciseaux') || (p2choice == 'Feuille' && p1choice == 'Pierre') || (p2choice == 'Ciseaux' && p1choice == 'Feuille'))
		{
			document.getElementById('rpsinstruction').innerHTML = player2Name + " a gagné cette manche!";
			document.getElementById('rpsinstruction').style.color = '#ffaaaa'
			p2score = p2score + 1
		}
		document.getElementById('score').innerHTML = p1score + '-' + p2score;
		document.getElementById('rematch').innerHTML = "<input type='button' onclick='newRound()' value='Revanche!'>"
	}
});

// New Round & New Challenger

function newRound()
{
	game = 1
	p1ready = 0
	p2ready = 0
	p1choice = ''
	p2choice = ''	
	
	document.getElementById('waitp1').innerHTML = "<strong>[En attente de " + player1Name + "]</strong>";
	document.getElementById('waitp2').innerHTML = "<strong>[En attente de " + player2Name + "]</strong>";
	document.getElementById('rematch').innerHTML = "<p></p>"
	document.getElementById('rpsinstruction').innerHTML = "Choisissez entre Pierre, Feuille et Ciseaux"
	document.getElementById('rpsinstruction').style.color = '#aaaaaa'
	document.getElementById('choosingP1').src = ""
	document.getElementById('choosingP2').src = ""
}

function newChallenger()
{
	newRound()
	p1score = 0
	p2score = 0
	document.getElementById('score').innerHTML = p1score + '-' + p2score;
}


// Name Functions yeehaw

function p1name()
{
	if (p1choice == '' && p2choice == '' && document.getElementById('p1').value != '' && document.getElementById('p1').value != player1Name) // Conditions différente du Morpion : On peut changer de noms avant que les 2 joueurs sont prêts
	{
		player1Name = document.getElementById('p1').value;
		alert("Nouveau Joueur 1 : " + player1Name);
		document.getElementById('waitp1').innerHTML = "<strong>[En attente de " + player1Name + "]</strong>";
		newChallenger()
	}
}

function p2name()
{
	if (p1choice == '' && p2choice == '' && document.getElementById('p2').value != '' && document.getElementById('p2').value != player2Name)
	{
		player2Name = document.getElementById('p2').value;
		alert("Nouveau Joueur 2 : " + player2Name);
		document.getElementById('waitp2').innerHTML = "<strong>[En attente de " + player2Name + "]</strong>";
		newChallenger()
	}
}